#include <VirtualWire.h>
#include "LowPower.h"
#include "DHT.h"
#define DHTPIN 8     // what pin we're connected to
#define TXPIN 12

#define DHTTYPE DHT22   // DHT 22
#define DEBUG 1

#define SENSOR_OUTSIDE 0x1

struct TxMsg {
  uint8_t sensor_id;
  int16_t humidity;
  int16_t temp;
  int16_t heat_index;
};

static uint8_t txBuf[24];
static const uint8_t txMsgSize = sizeof(TxMsg);

DHT dht(DHTPIN, DHTTYPE);

#if DEBUG
static char debug_buf[64];
#endif

void setup() {
#if DEBUG
  Serial.begin(9600);
#endif

  dht.begin();  // initialing the DHT sensor
  // VirtualWire setup
  vw_setup(2000); // Bits per sec
  vw_set_tx_pin(TXPIN);// Set the Tx pin. Default is 12
}

void loop() {
  TxMsg txMsg;
  txMsg.sensor_id = SENSOR_OUTSIDE;
  
  // Read and store Sensor Data
  txMsg.humidity = dht.readHumidity();
  txMsg.temp = dht.readTemperature();
  int f = dht.readTemperature(true);
  int hi_f = dht.computeHeatIndex(f, txMsg.humidity); //heat index in fahrenheit
  txMsg.heat_index = (hi_f - 32) * 5 / 9; // convert heat index to celcius
  
#if DEBUG
  snprintf(debug_buf, sizeof(debug_buf), "Hum: %d, temp: %d°C, heat idx: %d\n", txMsg.humidity, txMsg.temp, txMsg.heat_index);
  Serial.print(debug_buf);
#endif

  memset(txBuf, 0, sizeof(txBuf));
  memcpy(&txBuf[0], &txMsgSize, sizeof(txMsgSize));
  memcpy(&txBuf[1], &txMsg, sizeof(txMsg));

  vw_send((uint8_t *) txBuf, sizeof(txMsgSize) + sizeof(txMsg));
  vw_wait_tx(); // Wait until the whole message is gone
  
  LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
}
